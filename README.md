# MovieApp

This app displays a list of TV Shows and their details (2 screens), retrieved by a rest API.
The list is stored in the local database (cache mechanism) and is retrieved from the local storage.
Also, you can swipe to refresh the list.

Used:
Kotlin,
MVVM Architecture,
Room database,
Retrofit,
Dependency Injection (Hilt),
Flows - Coroutines,
RecyclerView,
Glide Library
