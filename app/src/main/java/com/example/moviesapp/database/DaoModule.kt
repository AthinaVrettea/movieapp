package com.example.moviesapp.database

import com.example.moviesapp.database.dao.MoviesDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object DaoModule {

    @Provides
    fun providesMoviesDao(database: MovieDatabase): MoviesDao = database.moviesDao()

}