package com.example.moviesapp.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.moviesapp.domain.Movie

@Entity(tableName = "movies")
data class MovieEntity(
    @PrimaryKey
    val id: Long,
    val thumbnail: String,
    val name: String,
    val rating: Double,
    val summary: String,
    val type: String,
    val language: String,
    val status: String,
    val runtime: Long
)

fun MovieEntity.asExternalModel() = Movie(
    id = id,
    thumbnail = thumbnail,
    name = name,
    rating = rating,
    summary = summary,
    type = type,
    language = language,
    status = status,
    runtime = runtime
)
