package com.example.moviesapp.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.moviesapp.database.dao.MoviesDao
import com.example.moviesapp.database.model.MovieEntity

@Database(entities = [MovieEntity::class], version = 1, exportSchema = true)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun moviesDao(): MoviesDao

}