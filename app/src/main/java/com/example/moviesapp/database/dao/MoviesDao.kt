package com.example.moviesapp.database.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import com.example.moviesapp.database.model.MovieEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface MoviesDao {

    @Query(value = "SELECT * FROM Movies")
    fun getMovieEntities(): Flow<List<MovieEntity>>

    @Upsert
    suspend fun upsertMovies(entities: List<MovieEntity>)

}