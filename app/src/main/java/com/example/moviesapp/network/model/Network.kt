package com.example.moviesapp.network.model


data class Network(
    val id: Long,
    val name: String,
    val country: Country,
    val officialSite: String?
)