package com.example.moviesapp.network.model


data class Schedule(
    val time: String,
    val days: List<String>
)