package com.example.moviesapp.network

import com.example.moviesapp.network.model.MoviesResponse
import retrofit2.http.GET

interface MoviesApi {

    @GET("shows")
    suspend fun getShows(): List<MoviesResponse>

}