package com.example.moviesapp.network.model

data class Self(
    val href: String
)