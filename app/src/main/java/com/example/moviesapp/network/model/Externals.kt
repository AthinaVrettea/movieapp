package com.example.moviesapp.network.model


data class Externals(
    val tvrage: Long,
    val thetvdb: Long?,
    val imdb: String?
)