package com.example.moviesapp.network.model

data class MoviesResponse(
    val id: Long,
    val url: String,
    val name: String,
    val type: String,
    val language: String,
    val genres: List<String>,
    val status: String,
    val runtime: Long?,
    val averageRuntime: Long,
    val premiered: String,
    val ended: String?,
    val officialSite: String?,
    val schedule: Schedule,
    val rating: Rating,
    val weight: Long,
    val network: Network?,
    val webChannel: WebChannel?,
    val dvdCountry: Country?,
    val externals: Externals,
    val image: Image,
    val summary: String,
    val updated: Long,
    val _links: Links
)