package com.example.moviesapp.network.model

data class NextEpisode(
    val href: String
)
