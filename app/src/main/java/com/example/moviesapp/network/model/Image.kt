package com.example.moviesapp.network.model


data class Image(
    val medium: String,
    val original: String
)