package com.example.moviesapp.network.model


data class PreviousEpisode(
    val href: String
)