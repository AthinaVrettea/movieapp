package com.example.moviesapp.network.model


data class Country(
    val name: String,
    val code: String,
    val timezone: String
)