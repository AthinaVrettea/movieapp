package com.example.moviesapp.network.model


data class Rating(
    val average: Double?
)