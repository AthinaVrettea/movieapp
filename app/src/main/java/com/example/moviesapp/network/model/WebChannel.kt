package com.example.moviesapp.network.model

data class WebChannel(
    val id: Long,
    val name: String,
    val country: Country?,
    val officialSite: String?
)
