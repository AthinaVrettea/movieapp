package com.example.moviesapp.network.model


data class Links(
    val self: Self,
    val previousepisode: PreviousEpisode,
    val nextepisode: NextEpisode?
)