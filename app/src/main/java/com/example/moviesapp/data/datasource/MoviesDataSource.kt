package com.example.moviesapp.data.datasource

import com.example.moviesapp.network.MoviesApi
import com.example.moviesapp.network.model.MoviesResponse
import javax.inject.Inject

class MoviesDataSource @Inject constructor(
    private val moviesApi: MoviesApi
) {
    suspend fun getMovies(): List<MoviesResponse> {
        return moviesApi.getShows()
    }

}