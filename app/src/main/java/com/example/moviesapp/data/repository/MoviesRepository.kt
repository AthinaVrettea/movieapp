package com.example.moviesapp.data.repository

import android.util.Log
import com.example.moviesapp.domain.Movie
import com.example.moviesapp.domain.Response
import com.example.moviesapp.data.datasource.MoviesDataSource
import com.example.moviesapp.database.dao.MoviesDao
import com.example.moviesapp.database.model.MovieEntity
import com.example.moviesapp.database.model.asExternalModel
import com.example.moviesapp.network.model.MoviesResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class MoviesRepository @Inject constructor(
    private val moviesDataSource: MoviesDataSource,
    private val moviesDao: MoviesDao
) {

    suspend fun fetchNetworkMovies(): Flow<Response<Any>> = flow {
        emit(Response.Loading)
        val moviesResponse = moviesDataSource.getMovies()

        sync(moviesResponse.map {
            it.toEntity()
        })
        emit(Response.Success(Any()))
    }.catch {
        emit(Response.Error("Error fetching network movies"))
    }

    fun fetchLocalMovies(): Flow<List<Movie>> =
        moviesDao.getMovieEntities().map {
            it.map { movieEntity ->
                movieEntity.asExternalModel()
            }
        }

    private suspend fun sync(movies: List<MovieEntity>) {
        try {
            moviesDao.upsertMovies(movies)
        } catch (e: Exception) {
            Log.d("MoviesRepository", e.message.toString())
        }
    }
}

fun MoviesResponse.toEntity() = MovieEntity(
    id = id,
    thumbnail = image.medium,
    name = name,
    rating = rating.average ?: 0.0,
    summary = summary,
    type = type,
    language = language,
    status = status,
    runtime = runtime ?: 0
)