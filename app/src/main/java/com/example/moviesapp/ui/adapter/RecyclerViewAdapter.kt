package com.example.moviesapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.domain.Movie

class RecyclerViewAdapter(
    private val context: Context,
    private val onMovieClickListener: OnMovieClickListener
) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    interface OnMovieClickListener {
        fun showDetails(movieId: Long)
    }

    var movieList: List<Movie> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return ViewHolder(view, onMovieClickListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val movie = movieList[position]

        Glide.with(context).load(movieList[position].thumbnail)
            .into(holder.movieThumbnail)
        holder.movieName.text = movie.name
        holder.movieRating.text = HtmlCompat.fromHtml(
            context.getString(R.string.movie_rating, movie.rating.toString()),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        holder.movieId = movie.id

    }

    override fun getItemCount(): Int {
        return movieList.size
    }

    class ViewHolder(itemView: View, onMovieClickListener: OnMovieClickListener) :
        RecyclerView.ViewHolder(itemView) {
        val movieThumbnail: ImageView = itemView.findViewById(R.id.movieThumbnail)
        val movieName: TextView = itemView.findViewById(R.id.movieName)
        val movieRating: TextView = itemView.findViewById(R.id.movieRating)
        val movieContainer: CardView = itemView.findViewById(R.id.movieContainer)
        var movieId: Long = 0

        init {
            movieContainer.setOnClickListener {
                onMovieClickListener.showDetails(movieId)
            }
        }
    }

}