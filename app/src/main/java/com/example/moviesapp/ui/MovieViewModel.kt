package com.example.moviesapp.ui

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moviesapp.data.repository.MoviesRepository
import com.example.moviesapp.domain.Movie
import com.example.moviesapp.domain.Response
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val moviesRepository: MoviesRepository
) : ViewModel() {

    private val _networkUiState = MutableStateFlow<NetworkUiState>(NetworkUiState.Loading)
    val networkUiState: StateFlow<NetworkUiState> = _networkUiState
    var movieState = MovieState(0)

    val movies: StateFlow<MovieUiState> = localMovies()
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5_000),
            initialValue = MovieUiState.Loading
        )

    init {
        fetchMovies()
    }

    private fun localMovies(): Flow<MovieUiState> {
        Log.d("MovieViewModel", "local movies")
        return moviesRepository.fetchLocalMovies().map {
            if (it != null) {
                MovieUiState.Success(it)
            } else {
                MovieUiState.Error(it)
            }
        }
    }

    fun fetchMovies() {
        viewModelScope.launch {
            moviesRepository.fetchNetworkMovies().collect { response ->
                _networkUiState.update {
                    when (response) {
                        is Response.Error -> NetworkUiState.Error(response.code.toString())
                        is Response.Success -> NetworkUiState.Success
                        Response.Loading -> NetworkUiState.Loading
                    }
                }
            }
        }
    }


    fun onSelectedItemEvent(movieId: Long) {
        movieState = movieState.copy(selectedMovieId = movieId)

    }

    fun getSelectedMovie(): Movie? {
        //Alternatively fetch movie from local database
        return if (movies.value is MovieUiState.Success) {
            (movies.value as MovieUiState.Success).movieList
                .find { movie -> movie.id == movieState.selectedMovieId }
        } else {
            null
        }
    }

}

data class MovieState(
    val selectedMovieId: Long
)

sealed interface MovieUiState {
    data class Success(val movieList: List<Movie>) : MovieUiState
    data class Error(val code: String) : MovieUiState
    object Loading : MovieUiState
}

sealed interface NetworkUiState {
    object Success : NetworkUiState
    data class Error(val code: String) : NetworkUiState
    object Loading : NetworkUiState
}