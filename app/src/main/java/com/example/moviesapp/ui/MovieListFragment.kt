package com.example.moviesapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviesapp.R
import com.example.moviesapp.databinding.FragmentMovieListBinding
import com.example.moviesapp.ui.adapter.RecyclerViewAdapter
import kotlinx.coroutines.launch

class MovieListFragment : Fragment(), RecyclerViewAdapter.OnMovieClickListener {

    private var _binding: FragmentMovieListBinding? = null
    private val movieViewModel: MovieViewModel by activityViewModels()
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMovieListBinding.inflate(inflater, container, false)
        val layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = RecyclerViewAdapter(requireContext(), this)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                movieViewModel.networkUiState.collect {
                    when (it) {
                        is NetworkUiState.Error -> {
                            binding.swipeToRefresh.isRefreshing = false
                        }
                        NetworkUiState.Loading -> {

                        }
                        is NetworkUiState.Success -> {
                            binding.swipeToRefresh.isRefreshing = false
                        }
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {

                movieViewModel.movies.collect {
                    when (it) {
                        is MovieUiState.Error -> {
                            binding.swipeToRefresh.isRefreshing = false
                        }
                        MovieUiState.Loading -> {

                        }
                        is MovieUiState.Success -> {
                            binding.swipeToRefresh.isRefreshing = false
                            (binding.recyclerView.adapter as RecyclerViewAdapter).movieList =
                                it.movieList
                        }
                    }
                }
            }
        }

        binding.swipeToRefresh.setOnRefreshListener {
            movieViewModel.fetchMovies()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun showDetails(movieId: Long) {
        movieViewModel.onSelectedItemEvent(movieId)
        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
    }

}