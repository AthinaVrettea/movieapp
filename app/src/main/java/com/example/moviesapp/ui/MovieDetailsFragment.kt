package com.example.moviesapp.ui

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.moviesapp.R
import com.example.moviesapp.databinding.FragmentMovieDetailsBinding

class MovieDetailsFragment : Fragment() {

    private var _binding: FragmentMovieDetailsBinding? = null
    private val movieViewModel: MovieViewModel by activityViewModels()
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMovieDetailsBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var selectedMovie = movieViewModel.getSelectedMovie()
        Log.d("movie", selectedMovie.toString())

        Glide.with(requireContext()).load(selectedMovie?.thumbnail)
            .into(binding.movieImage)

        binding.movieName.text = HtmlCompat.fromHtml(
            getString(R.string.movie_name, selectedMovie?.name),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        binding.movieRating.text = HtmlCompat.fromHtml(
            getString(R.string.movie_rating, selectedMovie?.rating.toString()),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        binding.movieSummary.text = HtmlCompat.fromHtml(
            getString(R.string.movie_summary, selectedMovie?.summary),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        binding.movieType.text = HtmlCompat.fromHtml(
            getString(R.string.movie_type, selectedMovie?.type),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        binding.movieLanguage.text = HtmlCompat.fromHtml(
            getString(R.string.movie_language, selectedMovie?.language),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        binding.movieStatus.text = HtmlCompat.fromHtml(
            getString(R.string.movie_status, selectedMovie?.status),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        binding.movieRuntime.text = HtmlCompat.fromHtml(
            getString(
                R.string.movie_runtime,
                selectedMovie?.runtime.toString()
            ), HtmlCompat.FROM_HTML_MODE_LEGACY
        )

        binding.buttonSecond.setOnClickListener {
            findNavController().navigate(R.id.action_SecondFragment_to_FirstFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}