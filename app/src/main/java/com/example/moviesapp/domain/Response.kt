package com.example.moviesapp.domain

sealed interface Response<out T> {
    data class Success<T>(val data: T) : Response<T>
    data class Error(val code: String? = null, val exception: Throwable? = null) : Response<Nothing>
    object Loading : Response<Nothing>
}