package com.example.moviesapp.domain


data class Movie(
    val id: Long,
    val thumbnail: String,
    val name: String,
    val rating: Double,
    val summary: String,
    val type: String,
    val language: String,
    val status: String,
    val runtime: Long
)
